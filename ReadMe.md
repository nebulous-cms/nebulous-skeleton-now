# Nebulous Skeleton #

A skeleton website you can clone, reuse and repurpose for your own websites.

## Author ##

Andrew Chilton.

## License ##

MIT.

Design and styles copyright Jeremy Thomas. Asknowledged below in the thanks.

## Thanks ##

Thanks to [Jeremy Thomas](https://twitter.com/jgthms) for https://jgthms.com/web-design-in-4-minutes/ for this minimlist design.

(Ends)
