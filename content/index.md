To create and serve up a Nebulous CMS website hosted on `now.sh` in under 3 mins, please continue reading.

Let's go!

## Pre-requisites ##

For this tutorial it is assumed:

1. you already have an account with Zeit
1. you have the `now` command line tool installed (see
   [Now CLI](https://zeit.co/docs/features/now-cli) if not)
1. you are familiar with both now.sh and Git

## Clone the Nebulous CMS Skeleton ##

Let's create a new website called `my-project`. We'll first get the skeleton code and check it runs locally:

```
$ git clone https://gitlab.com/nebulous-cms/nebulous-skeleton-now.git my-project
$ cd my-project
$ npm install
$ npm start
```

Next browse to `http://localhost:3000` and check if everything looks okay.

Next, edit the `package.json` file and change the `"name"` to be `"my-project"`. This name will become part of your
app's URL (e.g. `my-project-xxxxxxxxxx.now.sh`) and you are free to choose another name if you prefer.

Now it is time to deploy your website live to a new `now` app:

```
$ now
> Deploying /path/to/my-project under <your name here>

...etc...

> Success! Deployment ready
```

Awesome! Congratulations on deploying your first [Nebulous CMS](https://nebulous-cms.org/) website. If you like it so
far, then we'd love it if you could star the [Nebulous CMS](https://gitlab.com/nebulous-cms/nebulous-server) project
on GitLab.

## Change your Content ##

Head into your `content/` dir and create two new files called `about.md` and `about.json`. Type whatever you want into
`about.md` but please add the following (correctly) into `about.json`:

```
{
  "title": "About"
}
```

Restart your local Nebulous server and you should now be able to browse to the `/about` page in your new site.

## Further Instructions ##

Further information can be found on the main [Nebulous CMS](https://nebulous-cms.org/) website, or continue this
tutorial on the [Nebulous Intro](https://nebulous-cms.org/intro) page.

## Links ##

|               | Website                                     | Twitter                                               |
|:-------------:|:-------------------------------------------:|:-----------------------------------------------------:|
| Project       | [Nebulous CMS](https://nebulous-cms.org/)   | [@NebulousCMS](https://twitter.com/NebulousCMS)       |
| Company       | [Nebulous Design](https://nebulous.design/) | [@NebulousDesign](https://twitter.com/NebulousDesign) |
| Author        | [Andrew Chilton](https://chilts.org/)       | [@andychilton](https://twitter.com/andychilton)       |

## License ##

MIT.

(Ends)
